﻿namespace TaskManager.Domain.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using DM = TaskManager.Domain.Entities;
    public class Task : EntityBase
    {
        [MaxLength(256)]
        public string Description { get; set; }

        public bool IsDone { get; set; } = false;

        public int ListId { get; set; }

        [ForeignKey("ListId")]
        public virtual DM.List List { get; set; }
    }
}
