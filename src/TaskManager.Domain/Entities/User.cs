﻿namespace TaskManager.Domain.Entities
{
    using System.Collections.Generic;
    using DM = TaskManager.Domain.Entities;
    public class User : EntityBase
    {
        public string UserExtId { get; set; }

        public virtual ICollection<DM.List> Lists { get; set; }
             = new List<DM.List>(); // base initialization
    }
}
