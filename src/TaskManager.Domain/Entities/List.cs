﻿namespace TaskManager.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using DM = TaskManager.Domain.Entities;

    public class List : EntityBase
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; } // should be computed further

        public int UserId { get; set; }
        
        [ForeignKey("UserId")]
        public virtual DM.User User { get; set; }

        public virtual ICollection<DM.Task> Tasks { get; set; }
            = new List<DM.Task>(); // base initialization
    }
}
