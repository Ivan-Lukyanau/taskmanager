﻿namespace TaskManager.API
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using TaskManager.DAL.Context;
    using TaskManager.DAL;
    using TaskManager.DAL.Abstract;
    using Microsoft.Extensions.Configuration;
    using Microsoft.EntityFrameworkCore;
    using DM = TaskManager.Domain.Entities;
    using Microsoft.Extensions.Caching.Distributed;
    using TaskManager.API.Infrastructure;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Infrastructure;

    public class Startup
    {
        public static IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddMemoryCache();

            // using custom middleware to get access
            services.AddAuthorization(options =>
            {
                options.AddPolicy("TokenRequire",
                    policy => policy.Requirements.Add(new OnlyWithTokenRequirement()));
            });


            services.AddSingleton<IAuthorizationHandler, OnlyWithTokenRequirement>();
            
            /// Plug-in local redis cache
            //services.AddDistributedRedisCache(options =>
            //{
            //    options.Configuration = "localhost:6379";
            //    options.InstanceName = "";
            //});

            /// CORS
            ///
            services.AddCors(o => o.AddPolicy("AllowSpecific", builder =>
            {
                builder
                .WithOrigins(new string[] { "http://localhost:3001" /*UI*/, "http://localhost:3090" /*AUTH SERVICE*/})
                .AllowAnyMethod()
                .AllowAnyHeader();
            }));

            /// CONNECTIONS
            /// 
            var connectionString = Startup.Configuration["connectionStrings:taskInfoDBConnectionString"];
            services.AddDbContext<TaskInfoContext>(c => c.UseSqlServer(connectionString));

            /// DEPENDENCIES
            /// 
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IListRepository, ListRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<TokenUtils>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, TaskInfoContext taskInfoContext)
        {
            loggerFactory.AddConsole();

            if ( env.IsDevelopment() ) {
                app.UseDeveloperExceptionPage();
            } else if (env.IsProduction()) {
                app.UseExceptionHandler();
            }

            taskInfoContext.EnsureSeedDataForContext(); // analog SEED for database start population

            app.UseStatusCodePages();

            app.UseMvc();
            #region Automapper
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<DM.List, Models.List.ListWithoutTasksDto>();

                cfg.CreateMap<DM.Task, Models.Task.TaskDto>();
                cfg.CreateMap<Models.Task.TaskForCreationDto, DM.Task>();

                cfg.CreateMap<Models.Task.TaskForUpdateDto, DM.Task>();
                cfg.CreateMap<DM.Task, Models.Task.TaskForUpdateDto>();
                
                cfg.CreateMap<DM.List, Models.List.ListDto>(); 
                cfg.CreateMap<Models.List.ListForCreationDto, DM.List>();

            });
            #endregion // Automapper
        }
    }
}
