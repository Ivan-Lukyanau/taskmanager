﻿namespace TaskManager.API.Models.Auth
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Token { get; set; }
    }
}
