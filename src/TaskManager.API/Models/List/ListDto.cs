﻿namespace TaskManager.API.Models.List
{
    using System;
    using System.Collections.Generic;

    public class ListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public int NumberOfTasks { get { return Tasks.Count; } }
        public ICollection<Models.Task.TaskDto> Tasks { get; set; }
        = new List<Models.Task.TaskDto>();
    }
}
