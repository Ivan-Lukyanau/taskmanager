﻿namespace TaskManager.API.Models.List
{
    using System.ComponentModel.DataAnnotations;

    public class ListForCreationDto
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
