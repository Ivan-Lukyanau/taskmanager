﻿namespace TaskManager.API.Models.List
{
    using System;

    public class ListWithoutTasksDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
