﻿namespace TaskManager.API.Models.Task
{
    using System.ComponentModel.DataAnnotations;

    public class TaskForUpdateDto
    {
        [Required]
        [MaxLength(256)]
        public string Description { get; set; }
        public bool IsDone { get; set; }
    }
}
