﻿namespace TaskManager.API.Models.Task
{    
    using System.ComponentModel.DataAnnotations;

    public class TaskForCreationDto
    {
        [Required]
        [MaxLength(256)]
        public string Description { get; set; }
    }
}
