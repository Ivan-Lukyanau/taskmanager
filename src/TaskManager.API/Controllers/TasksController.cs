﻿namespace TaskManager.API.Controllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using TaskManager.API.Models.Task;
    using TaskManager.DAL.Abstract;
    using DM = TaskManager.Domain.Entities;

    [Route("api/lists")]
    [EnableCors("AllowSpecific")]
    public class TasksController : Controller
    {
        private readonly IListRepository _listRepository;
        public TasksController(IListRepository listRepository) {
            _listRepository = listRepository;
        }
        [HttpGet("{listId}/tasks")]
        public IActionResult GetTasks(int listId) {
            if ( !_listRepository.IsExists(listId) ) {
                return NotFound();
            }

            var tasksForList = _listRepository.GetTasksForList(listId);
            var result = Mapper.Map<IEnumerable<TaskDto>>(tasksForList);

            return Ok(result);
        }

        [HttpGet("{listId}/tasks/{id}", Name = "GetTask")]
        public IActionResult GetTask(int listId, int id) {
            if ( !_listRepository.IsExists(listId) ) {
                return NotFound();
            }

            var foundTask = _listRepository.GetTaskForList(listId, id);
            if ( foundTask == null ) {
                return NotFound();
            }

            var result = Mapper.Map<TaskDto>(foundTask);

            return Ok(result);
        }

        [HttpPost("{listId}/tasks")]
        public IActionResult CreateTask(int listId, [FromBody]TaskForCreationDto model) {
            if ( model == null ) {
                return BadRequest(); // 400
            }

            if ( !ModelState.IsValid ) {                
                return BadRequest(ModelState);
            }

            if ( !_listRepository.IsExists(listId) ) {
                return NotFound();
            }

            var targetModel = Mapper.Map<DM.Task>(model);

            _listRepository.AddTaskForCity(listId, targetModel);

            if ( !_listRepository.Save() ) {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var taskToReturn = Mapper.Map<TaskDto>(targetModel);

            // this result contains CREATION header that specify uri where the new entry can be found
            return CreatedAtRoute("GetTask", new {
                listId = listId,
                id = taskToReturn.Id
            }, taskToReturn); // 201 - Created
        }

        [HttpDelete("{listId}/tasks/{id}")]
        public IActionResult DeleteTask(int listId, int id) {
            if ( !_listRepository.IsExists(listId) ) {
                return NotFound();
            }

            var foundTask = _listRepository.GetTaskForList(listId, id);

            if ( foundTask == null ) {
                return NotFound();
            }

            _listRepository.DeleteTask(foundTask);

            if ( !_listRepository.Save() ) {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

        [HttpPatch("{listId}/tasks/{id}")]
        public IActionResult PartiallyUpdateTask(int listId, int id, [FromBody]JsonPatchDocument<TaskForUpdateDto> model) {
            if ( model == null ) {
                BadRequest();
            }

            if ( !ModelState.IsValid ) {
                return BadRequest(ModelState);
            }
            
            if ( !_listRepository.IsExists(listId) ) {
                return NotFound();
            }

            var foundTask = _listRepository.GetTaskForList(listId, id);
            if ( foundTask == null ) {
                return NotFound();
            }

            var taskToPatch = Mapper.Map<TaskForUpdateDto>(foundTask);

            model.ApplyTo(taskToPatch, ModelState);
            if ( !ModelState.IsValid ) {
                return BadRequest(ModelState);
            }

            if ( string.IsNullOrEmpty(taskToPatch.Description)) {
                ModelState.AddModelError("Description", "The provided description should not be null or empty.");
            }

            TryValidateModel(taskToPatch);
            if ( !ModelState.IsValid ) {
                return BadRequest(ModelState);
            }

            Mapper.Map(taskToPatch, foundTask);

            if ( !_listRepository.Save() ) {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();

        }

    }
}
