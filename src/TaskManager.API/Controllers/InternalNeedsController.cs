﻿namespace TaskManager.API.Controllers
{
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Memory;
    using System;
    using System.Diagnostics;

    [Route("api/guard")]
    [EnableCors("AllowSpecific")]
    public class InternalNeedsController : Controller
    {
        private readonly IMemoryCache _cache;

        public InternalNeedsController(IMemoryCache cache) {
            _cache = cache; 
        }
        
        [HttpGet("tokens/{key}")]
        public IActionResult GetTokenFromCacheByKey(string key) {
            string cacheEntry = string.Empty;

            if ( !_cache.TryGetValue(key, out cacheEntry) ) {
                cacheEntry = "Cache is expired or not available";
            }

            return Ok(cacheEntry);
        }

        /// We will use it from other service if user was signed out there
        [HttpDelete("tokens/{key}")]
        public IActionResult DeleteTokenFromCache(string key, [FromBody]string token) {
            bool result = true;

            try {
                _cache.Remove(key);
            }
            catch ( Exception ex) {

                Debug.WriteLine("Cannot execute Remove operetion. Details: ", ex.Message);
                result = false;
            }

            return Ok(result ? "Token successfully deleted" : "Something went wrong while request handling.");
        }

        [HttpPost("tokens")]
        public IActionResult InsertTokenIntoServerCache([FromBody]Models.Auth.UserInfo model) {

            if ( model == null ) {
                return BadRequest();
            }

            try {
                string isExistsInCache = null;
                // if token with current user key already exists just rewrite values of this entry
                // with expire date equals 24 hours
                _cache.TryGetValue(model.Id, out isExistsInCache);

                if ( !string.IsNullOrEmpty(isExistsInCache) ) {
                    _cache.Set(model.Id, model.Token, (DateTime.Now.AddHours(24) - DateTime.Now));
                } else {
                    var cacheEntry = _cache.GetOrCreate(model.Id, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromHours(24);
                        return model.Token;
                    });
                }
            }
            catch ( Exception ex ) {

                Debug.WriteLine(ex.Message); // logger here
                return StatusCode(500, "Something went wrong while request handling.");
            }

            return Ok(true);    
        }
    }
}
