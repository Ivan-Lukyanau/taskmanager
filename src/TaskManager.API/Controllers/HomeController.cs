﻿namespace TaskManager.API.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using System;
    using System.Text;

    /// <summary>
    /// Just to try cached functions
    /// </summary>
    public class HomeController : Controller
    {
        IDistributedCache _memoryCache;
        public HomeController(IDistributedCache memoryCache) {
            _memoryCache = memoryCache;
        }
        [Route("home/SetCacheData")]
        public IActionResult SetCacheData() {
            var Time = DateTime.Now.ToLocalTime().ToString();
            var cacheOptions = new DistributedCacheEntryOptions {
                AbsoluteExpiration = DateTime.Now.AddYears(1)
            };
            _memoryCache.Set("Time", Encoding.UTF8.GetBytes(Time), cacheOptions);
            return View();
        }
        [Route("home/GetCacheData")]
        public IActionResult GetCacheData() {
            string Time = string.Empty;
            Time = Encoding.UTF8.GetString(_memoryCache.Get("Time"));
            ViewBag.data = Time;
            return View();
        }
        [Route("home/RemoveCacheData")]
        public bool RemoveCacheData() {
            _memoryCache.Remove("Time");
            return true;
        }
    }
}
