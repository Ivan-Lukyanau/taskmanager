﻿namespace TaskManager.API.Controllers
{
    using AutoMapper;
    using DAL.Abstract;
    using Infrastructure;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Models.List;
    using System.Collections.Generic;
    using System.Diagnostics;

    [Route("api/lists")]
    [EnableCors("AllowSpecific")]
    [Authorize(Policy = "TokenRequire")]
    public class ListOfTasksController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IListRepository _listRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMemoryCache _memcache;
        //private readonly IDistributedCache _cache;
        private readonly TokenUtils _tokenUtils;
        private readonly string _userId;

        public ListOfTasksController(
                                        IHttpContextAccessor httpContextAccessor,
                                        IListRepository listRepository,
                                        IUserRepository userRepository,
                                        IMemoryCache memcache,
                                        /*IDistributedCache cache,*/
                                        TokenUtils tokenUtils) {
            _httpContextAccessor = httpContextAccessor;
            _listRepository = listRepository;
            _userRepository = userRepository;
            _memcache = memcache;
            /*_cache = cache;*/
            _tokenUtils = tokenUtils;
            _userId = this.CheckCurrentIdentity(); // check id from request
        }
        private string CheckCurrentIdentity() {
            var userIdFromRequest = string.Empty;
            string tokenInRequestHeader =  _httpContextAccessor.HttpContext.Request.Headers["authorization"];
            
            if ( !string.IsNullOrEmpty(tokenInRequestHeader) ) {
                userIdFromRequest = _tokenUtils.ExtractUserId(tokenInRequestHeader);
            }
            //if ( !string.IsNullOrEmpty(User.Identity.Name) ) { // is undefined
            //    Debug.WriteLine("User Name from Identity:");
            //    Debug.WriteLine(User.Identity.Name);
            //}
            if ( !string.IsNullOrEmpty(userIdFromRequest) ) {

                Debug.WriteLine("User Id from Request Header:");
                Debug.WriteLine(userIdFromRequest);
            }

            return userIdFromRequest;
        }
        
        [HttpGet]
        [ResponseCache(Duration = 180, Location = ResponseCacheLocation.Any)]
        public IActionResult GetAllLists() {
            var allUsersLists = _listRepository.GetListsForUser(_userId);

            var result = Mapper.Map<IEnumerable<ListWithoutTasksDto>>(allUsersLists);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetListOfTasks")]
        public IActionResult GetList(int id, bool includeTasks = false) {

            if ( !_listRepository.IsExists(id) ) {
                return NotFound();
            }
            var list = _listRepository.GetList(id, includeTasks);
            
            if ( includeTasks ) {
                var result = Mapper.Map<ListDto>(list);
                return Ok(result);
            } else {
                var result = Mapper.Map<ListWithoutTasksDto>(list);
                return Ok(result);
            }            
        }

        [HttpPost]
        public IActionResult CreateList([FromBody]ListForCreationDto model) {
            if ( model == null) {
                return BadRequest(); // 400
            }
            if ( !ModelState.IsValid ) {
                return BadRequest(ModelState); // 400
            }

            // if we didn't get userId through the auth header from request
            if ( string.IsNullOrEmpty(_userId) ) {
                return BadRequest();
            }

            // get user for who we are creating the list
            var user = _userRepository.GetByExternalId(_userId);
            if ( user == null ) {
                return NotFound();
            }
            
            var listToInsert = Mapper.Map<Domain.Entities.List>(model);
            // assign listToInsert with found user we are working with
            listToInsert.UserId = user.Id;

            _listRepository.Insert(listToInsert);

            if ( !_listRepository.Save()) {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var returnedListDto = Mapper.Map<ListWithoutTasksDto>(listToInsert);

            return CreatedAtRoute("GetListOfTasks", new {
                id = returnedListDto.Id
            }, returnedListDto);

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteList(int id) {
            if ( !_listRepository.IsExists(id) ) {
                return NotFound();
            }

            var list = _listRepository.GetList(id);

            _listRepository.Delete(list);

            if ( !_listRepository.Save() ) {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }
    }
}
