﻿namespace TaskManager.API.Infrastructure
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.Extensions.Caching.Memory;
    using System.Threading.Tasks;
    using System.IdentityModel.Tokens.Jwt;
    using System.Diagnostics;

    public class OnlyWithTokenRequirement : AuthorizationHandler<OnlyWithTokenRequirement>, IAuthorizationRequirement
    {
        private readonly IMemoryCache _cache;
        
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OnlyWithTokenRequirement requirement) {

            var mvcContext = context.Resource as AuthorizationFilterContext;
            string tokenInHeader = mvcContext.HttpContext.Request.Headers["authorization"];

            var cache = ( IMemoryCache ) mvcContext.HttpContext.RequestServices.GetService(typeof(IMemoryCache));
                        
            if ( string.IsNullOrEmpty(tokenInHeader) ) {
                (( AuthorizationHandlerContext ) context).Fail();
            } else {

                if(this.ValidToken(tokenInHeader, cache) ) {
                    (( AuthorizationHandlerContext ) context).Succeed(requirement);
                } else {
                    (( AuthorizationHandlerContext ) context).Fail();
                }
            }

            return Task.CompletedTask;
        }
        
        private bool ValidToken(string externalToken, IMemoryCache cache) {
            // 'HS256'
            var token = new JwtSecurityToken(jwtEncodedString: externalToken);

#if DEBUG
            Debug.WriteLine("Subject : ");
            Debug.WriteLine(token.Subject);
#endif

            // get users id from header authorization token // e.g. 123456
            var subjectId = token.Subject; 

            string result = null; // value by key 123456

            // try to find token in server cache
            var whetherInCacheUsersToken = cache.TryGetValue(subjectId, out result);

            if ( result != null) {
                // value by token id exists in the database so we can assume everything is fine
                return true;
            }

            return false;
        }
    }
}

