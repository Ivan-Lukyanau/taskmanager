﻿namespace TaskManager.API.Infrastructure
{
    using System.IdentityModel.Tokens.Jwt;

    public class TokenUtils
    {
        public string ExtractUserId(string externalToken) {
            // 'HS256' parsed toke
            var token = new JwtSecurityToken(jwtEncodedString: externalToken);
            
            // get users id from header authorization token : subject // e.g. 123456
                                    
            return token.Subject;
        }

    }
}
