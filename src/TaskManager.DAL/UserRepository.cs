﻿namespace TaskManager.DAL
{
    using Context;
    using System.Linq;
    using TaskManager.DAL.Abstract;
    using DM = TaskManager.Domain.Entities;

    public class UserRepository : Repository<DM.User>, IUserRepository
    {
        public UserRepository(TaskInfoContext context) : base(context) {

        }
        public DM.User GetByExternalId(string extUserId) {
            return Context.Set<DM.User>().FirstOrDefault(x => x.UserExtId == extUserId);
        }
    }
}
