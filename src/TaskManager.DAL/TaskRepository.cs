﻿namespace TaskManager.DAL
{
    using Abstract;
    using DM = Domain.Entities;
    using Context;

    public class TaskRepository : Repository<DM.Task>, ITaskRepository
    {
        public TaskRepository(TaskInfoContext context) : base(context) {

        }
        
    }
}
