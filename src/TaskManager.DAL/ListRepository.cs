﻿namespace TaskManager.DAL
{
    using System.Collections.Generic;
    using Abstract;
    using Microsoft.EntityFrameworkCore;
    using DM = TaskManager.Domain.Entities;
    using Context;
    using System.Linq;

    public class ListRepository : Repository<DM.List>, IListRepository
    {
        public ListRepository(TaskInfoContext context) : base(context) {

        }
        public IEnumerable<DM.List> GetListsForUser(string userId) {

            return (( TaskInfoContext ) Context).Lists
                .Where(x => x.User.UserExtId == userId)
                .AsEnumerable<DM.List>();

        }

        public DM.List GetList(int listId, bool includeTasks) {

            if ( includeTasks ) {
                return (( TaskInfoContext ) Context).Lists.Include(x => x.Tasks)
                    .Where(x => x.Id == listId)
                    .FirstOrDefault();
            }

            return base.Get(listId);
        }
        
        public IEnumerable<DM.Task> GetTasksForList(int listId) {
            return (( TaskInfoContext ) Context).Tasks.Where(x => x.ListId == listId).ToList();
        }

        public DM.Task GetTaskForList(int listId, int taskId) {
            return (( TaskInfoContext ) Context).Tasks.FirstOrDefault(x => x.ListId == listId && x.Id == taskId);
        }
        
        public void AddTaskForCity(int listId, DM.Task task) {
            var list = GetList(listId, false);
            list.Tasks.Add(task);
        }

        public void DeleteTask(DM.Task foundTask) {
            this.Context.Set<DM.Task>().Remove(foundTask);
        }
    }
}
