﻿namespace TaskManager.DAL.Context
{
    using Microsoft.EntityFrameworkCore;
    using DM = TaskManager.Domain.Entities;

    public class TaskInfoContext : DbContext
    {
        public TaskInfoContext(DbContextOptions<TaskInfoContext> options)
            : base(options) {
            Database.Migrate(); // if there is not database yet, it will create it.
        }

        public DbSet<DM.List> Lists { get; set; }
        public DbSet<DM.Task> Tasks { get; set; }
        public DbSet<DM.User> Users { get; set; }
    }
}
