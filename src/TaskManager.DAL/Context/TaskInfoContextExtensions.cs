﻿namespace TaskManager.DAL.Context
{
    using System.Collections.Generic;
    using System.Linq;
    using DM = TaskManager.Domain.Entities;
    public static class TaskInfoContextExtensions
    {
        public static void EnsureSeedDataForContext(this TaskInfoContext context) {

            if ( context.Lists.Any() ) {
                return;
            }

            // populate data if not exists
            var user = new Domain.Entities.User {
                UserExtId = "58c946af46a545abf8f51193",
                Lists = new List<DM.List> {
                    new DM.List {
                        Name = "First List",
                        Tasks = new List<DM.Task> {
                            new DM.Task { Description = "some description for first task" },
                            new DM.Task { Description = "some description for second task" },
                            new DM.Task { Description = "some description for third task" }
                        }
                    },
                    new DM.List {
                        Name = "Second List",
                        Tasks = new List<DM.Task> {
                            new DM.Task { Description = "some description for fourth task" },
                            new DM.Task { Description = "some description for fifth task" },
                            new DM.Task { Description = "some description for sixth task" }
                        }
                    }
                }
            };
            
            context.Users.Add(user);
            context.SaveChanges();
        }

    }
}
