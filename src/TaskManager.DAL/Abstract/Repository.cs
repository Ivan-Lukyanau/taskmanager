﻿namespace TaskManager.DAL.Abstract
{
    using TaskManager.Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;

    public class Repository<T> : IRepository<T> where T : EntityBase
    {
        // define here context filed and ctor 
        private readonly DbContext _context;

        protected DbContext Context { get { return _context; } }

        public Repository(DbContext context) {
            _context = context;
        }

        public IEnumerable<T> GetAll() {
            return _context.Set<T>().ToList();
        }

        public T Get(int id) {
            return _context.Set<T>().FirstOrDefault(x => x.Id == id);
        }

        public bool IsExists(int id) {
            return this.Get(id) != null;
        }

        public void Insert(T entity) {
            _context.Set<T>().Add(entity);
        }

        public void Delete(T entity) {
            _context.Remove<T>(entity);
        }

        public bool Save() {
            return (_context.SaveChanges() >= 0);
        }
    }
}
