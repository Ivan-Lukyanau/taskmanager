﻿namespace TaskManager.DAL.Abstract
{
    using DM = Domain.Entities;
    public interface IUserRepository
    {
        DM.User GetByExternalId(string extUserId);
    }
}
