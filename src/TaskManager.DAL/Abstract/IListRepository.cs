﻿namespace TaskManager.DAL.Abstract
{
    using System.Collections.Generic;
    using DM = TaskManager.Domain.Entities;
    public interface IListRepository : IRepository<DM.List>
    {
        DM.List GetList(int listId, bool includeTasks = false);
        IEnumerable<DM.Task> GetTasksForList(int listId);
        DM.Task GetTaskForList(int listId, int taskId);
        void AddTaskForCity(int listId, DM.Task task);
        void DeleteTask(DM.Task foundTask);

        IEnumerable<DM.List> GetListsForUser(string userId);
    }
}
