﻿namespace TaskManager.DAL.Abstract
{
    using System.Collections.Generic;
    using DM = TaskManager.Domain.Entities;
    public interface ITaskRepository : IRepository<DM.Task>
    {
    }
}
