﻿namespace TaskManager.DAL.Abstract
{
    using System.Collections.Generic;
    using TaskManager.Domain.Entities;

    public interface IRepository<T> where T : EntityBase
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        bool IsExists(int id);
        void Insert(T entity);
        void Delete(T entity);
        bool Save();
    }
}
